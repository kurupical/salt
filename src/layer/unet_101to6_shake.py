from keras.models import Model, load_model
from keras.layers import Input,Dropout,BatchNormalization,Activation,Add,Lambda,multiply
from keras.layers.core import Lambda
from keras.layers.convolutional import Conv2D, Conv2DTranspose
from keras.layers.pooling import MaxPooling2D
from keras.layers.merge import concatenate
from keras import backend as K
import tensorflow as tf

# class Shake(Layer):

def residual_block(block_input, num_filters=16, activation="relu"):
    x = BatchNormalization()(block_input)
    x = Activation(activation)(x)
    x = Conv2D(num_filters, (3, 3), padding="same")(x)
    x = BatchNormalization()(x)
    x = Activation(activation)(x)
    x = Conv2D(num_filters, (3, 3), padding="same")(x)
    x = Add()([x, block_input])
    return x


def shake_block(block_input, num_filters=16, activation="relu"):
    def substract(x1, x2):
        minus_x2 = Lambda(lambda x: -x)(x2)
        return Add()([x1, minus_x2])

    def shake(x1, x2):
        x1_ratio = K.random_uniform_variable(shape=tf.shape(x1), low=0, high=1)
        x2_ratio = substract(K.ones(tf.shape(x1)), x2)
        x1_mul = multiply([x1, x1_ratio])
        x2_mul = multiply([x2, x2_ratio])
        return Add()([x1_mul, x2_mul])

    x1 = residual_block(block_input, num_filters, activation)
    x2 = residual_block(block_input, num_filters, activation)
    x = shake(x1, x2)
    return x

def build_model(size, start_neurons, activation, dropout_ratio, optimizer):

    input_layer = Input((size, size, 1))

    # standard size 128->64 custom 101->50
    conv1 = shake_block(input_layer, start_neurons * 1)
    pool1 = MaxPooling2D((2, 2))(conv1)
    pool1 = Dropout(dropout_ratio / 2)(pool1)

    # standard size 64->32 custom 50->25
    conv2 = Conv2D(start_neurons * 2, (3, 3), padding="same")(pool1) # フィルタの枚数を揃えるため
    conv2 = shake_block(conv2, start_neurons * 2)
    pool2 = MaxPooling2D((2, 2))(conv2)
    pool2 = Dropout(dropout_ratio)(pool2)

    # standard size 32->16 custom 25->12
    conv3 = Conv2D(start_neurons * 4, (3, 3), padding="same")(pool2)
    conv3 = shake_block(conv3, start_neurons * 4)
    pool3 = MaxPooling2D((2, 2))(conv3)
    pool3 = Dropout(dropout_ratio)(pool3)

    # standard size 16->8 custom 12->6
    conv4 = Conv2D(start_neurons * 8, (3, 3), padding="same")(pool3)
    conv4 = shake_block(conv4, start_neurons * 8)
    pool4 = MaxPooling2D((2, 2))(conv4)
    pool4 = Dropout(dropout_ratio)(pool4)

    # Middle
    convm = Conv2D(start_neurons * 16, (3, 3), padding="same")(pool4)
    convm = shake_block(convm, start_neurons * 16)

    # standard 8->16, custom 6->12
    deconv4 = Conv2DTranspose(start_neurons * 8, (3, 3), strides=(2, 2), padding="same")(convm)
    uconv4 = concatenate([deconv4, conv4])
    uconv4 = Dropout(dropout_ratio)(uconv4)
    uconv4 = Conv2D(start_neurons * 8, (3, 3), padding="same")(uconv4)
    uconv4 = shake_block(uconv4, start_neurons * 8)

    # standard 16->32 custom 12->25
    deconv3 = Conv2DTranspose(start_neurons * 4, (3, 3), strides=(2, 2), padding="valid")(uconv4)
    uconv3 = concatenate([deconv3, conv3])
    uconv3 = Dropout(dropout_ratio)(uconv3)
    uconv3 = Conv2D(start_neurons * 4, (3, 3), padding="same")(uconv3)
    uconv3 = shake_block(uconv3, start_neurons * 4)

    # standard 32->64 custom 25->50
    deconv2 = Conv2DTranspose(start_neurons * 2, (3, 3), strides=(2, 2), padding="same")(uconv3)
    uconv2 = concatenate([deconv2, conv2])
    uconv2 = Dropout(dropout_ratio)(uconv2)
    uconv2 = Conv2D(start_neurons * 2, (3, 3), padding="same")(uconv2)
    uconv2 = shake_block(uconv2, start_neurons * 2)

    # standard 64->128 custom 50->101
    deconv1 = Conv2DTranspose(start_neurons * 1, (3, 3), strides=(2, 2), padding="valid")(uconv2)
    uconv1 = concatenate([deconv1, conv1])
    uconv1 = Dropout(dropout_ratio)(uconv1)
    uconv1 = Conv2D(start_neurons * 1, (3, 3), padding="same")(uconv1)
    uconv1 = shake_block(uconv1, start_neurons * 1)

    uconv1 = Dropout(dropout_ratio / 2)(uconv1)

    output_layer = Conv2D(1, (1, 1), padding="same", activation="sigmoid")(uconv1)

    model = Model(input_layer, output_layer)
    model.compile(loss="binary_crossentropy", optimizer=optimizer, metrics=["accuracy"])

    return model