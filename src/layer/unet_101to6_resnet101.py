from keras.models import Model, load_model
from keras.layers import Input,Dropout,BatchNormalization,Activation,Add
from keras.layers.core import Lambda
from keras.layers.convolutional import Conv2D, Conv2DTranspose
from keras.layers.pooling import MaxPooling2D
from keras.layers.merge import concatenate

from src.layer.loss import get_loss
from src.layer.optimizer import get_optimizer

from src.layer.common import *

import numpy as np
import tensorflow as tf
from keras import backend as K
from keras.regularizers import l2

ACTIVATION = "relu"


def residual_block(block_input, num_filters=16, is_first_layer=False, is_decode=False):
    def basic_block(x):
        for i in range(2):
            if is_first_layer and i == 0:
                strides = 1
                padding = "same"
            else:
                strides = 1
                padding = "same"

            x = BatchNormalization()(x)
            x = Activation(ACTIVATION)(x)
            x = Conv2D(num_filters, (3, 3), strides=strides, padding=padding)(x)
        return x

    x = basic_block(block_input)

    if K.int_shape(x) == K.int_shape(block_input):
        return Add()([x, block_input])
    else:
        # block_inputの形をxにあわせる
        if is_decode:
            w_x = Conv2D(num_filters, (3, 3), padding="same")(block_input)
        else:
            w_x = Conv2D(num_filters, (3, 3), strides=1, padding="same")(block_input)
        return Add()([x, w_x])


def build_model(size, start_neurons, activation, dropout_ratio, optimizer, loss):
    """

    :param size:
    :param start_neurons: paper use 64.
    :param activation:
    :param dropout_ratio:
    :param optimizer:
    :param loss:
    :return:
    """

    input_layer = Input((size, size, 1))

    conv1 = Conv2D(start_neurons, (7, 7), padding="same", strides=2)(input_layer)

    # standard size 128->64 custom 101->50
    # conv1 = MaxPooling2D((3, 3), strides=2, padding="same")(conv1)
    conv1 = residual_block(conv1, start_neurons * 1)
    conv1 = residual_block(conv1, start_neurons * 1)
    drop1 = MaxPooling2D((2, 2))(conv1)
    drop1 = Dropout(dropout_ratio / 2)(drop1)

    # standard size 64->32 custom 50->25
    conv2 = residual_block(drop1, start_neurons * 2, is_first_layer=True)
    conv2 = residual_block(conv2, start_neurons * 2)
    conv2 = residual_block(conv2, start_neurons * 2)
    conv2 = residual_block(conv2, start_neurons * 2)
    drop2 = MaxPooling2D((2, 2))(conv2)
    drop2 = Dropout(dropout_ratio)(drop2)

    # standard size 32->16 custom 25->12
    conv3 = residual_block(drop2, start_neurons * 4, is_first_layer=True)
    conv3 = residual_block(conv3, start_neurons * 4)
    conv3 = residual_block(conv3, start_neurons * 4)
    conv3 = residual_block(conv3, start_neurons * 4)
    conv3 = residual_block(conv3, start_neurons * 4)
    conv3 = residual_block(conv3, start_neurons * 4)
    drop3 = MaxPooling2D((2, 2))(conv3)
    drop3 = Dropout(dropout_ratio)(drop3)

    # Middle
    # standard size 16->8 custom 12->6
    convm = residual_block(drop3, start_neurons * 8, is_first_layer=True)
    convm = residual_block(convm, start_neurons * 8)
    convm = residual_block(convm, start_neurons * 8)

    # standard 16->32 custom 12->25
    deconv3 = Conv2DTranspose(start_neurons * 4, (3, 3), strides=(2, 2), padding="same")(convm)
    uconv3 = concatenate([deconv3, conv3])
    uconv3 = Dropout(dropout_ratio)(uconv3)
    uconv3 = residual_block(uconv3, start_neurons * 4, is_decode=True)
    uconv3 = residual_block(uconv3, start_neurons * 4)
    uconv3 = residual_block(uconv3, start_neurons * 4)
    uconv3 = residual_block(uconv3, start_neurons * 4)
    uconv3 = residual_block(uconv3, start_neurons * 4)
    uconv3 = residual_block(uconv3, start_neurons * 4)

    # standard 32->64 custom 25->50
    deconv2 = Conv2DTranspose(start_neurons * 2, (3, 3), strides=(2, 2), padding="same")(uconv3)
    uconv2 = concatenate([deconv2, conv2])
    uconv2 = Dropout(dropout_ratio)(uconv2)
    uconv2 = residual_block(uconv2, start_neurons * 2, is_decode=True)
    uconv2 = residual_block(uconv2, start_neurons * 2)
    uconv2 = residual_block(uconv2, start_neurons * 2)
    uconv2 = residual_block(uconv2, start_neurons * 2)

    # standard 64->128 custom 50->101
    deconv1 = Conv2DTranspose(start_neurons * 1, (3, 3), strides=(2, 2), padding="same")(uconv2)
    uconv1 = concatenate([deconv1, conv1])
    uconv1 = Dropout(dropout_ratio)(uconv1)
    uconv1 = residual_block(uconv1, start_neurons * 1, is_decode=True)
    uconv1 = residual_block(uconv1, start_neurons * 1)
    uconv1 = residual_block(uconv1, start_neurons * 1)

    uconv1 = Conv2DTranspose(start_neurons * 1, (3, 3), strides=(2, 2), padding="same")(uconv1)
    uconv1 = Conv2DTranspose(start_neurons * 1, (7, 7), strides=(2, 2), padding="same")(uconv1)
    if loss == "binary_crossentropy":
        output_layer = Conv2D(1, (1, 1), padding="same", activation="sigmoid")(uconv1)
    else:
        output_layer = Conv2D(1, (1, 1), padding="same")(uconv1)

    model = Model(input_layer, output_layer)
    model.compile(loss=get_loss(loss), optimizer=get_optimizer(optimizer), metrics=["accuracy", my_iou_metric])

    return model