import tensorflow as tf
import numpy as np
from keras import backend as K
from keras.models import Model, load_model
from keras.layers import Input,Dropout,BatchNormalization,Activation,Add
from keras.layers.core import Lambda
from keras.layers.convolutional import Conv2D, Conv2DTranspose
from keras.layers.pooling import MaxPooling2D
from keras.layers.merge import concatenate

def mean_iou(y_true, y_pred):
    prec = []
    for t in np.arange(0.5, 1.0, 0.05):
        y_pred_ = tf.to_int32(y_pred > t)
        score, up_opt = tf.metrics.mean_iou(y_true, y_pred_, 2)
        K.get_session().run(tf.local_variables_initializer())
        with tf.control_dependencies([up_opt]):
            score = tf.identity(score)
        prec.append(score)
    return K.mean(K.stack(prec), axis=0)


def get_iou_vector(A, B):
    batch_size = A.shape[0]
    metric = []
    for batch in range(batch_size):
        t, p = A[batch] > 0, B[batch] > 0

        intersection = np.logical_and(t, p)
        union = np.logical_or(t, p)
        iou = (np.sum(intersection > 0) + 1e-10) / (np.sum(union > 0) + 1e-10)
        thresholds = np.arange(0.5, 1, 0.05)
        s = []
        for thresh in thresholds:
            s.append(iou > thresh)
        metric.append(np.mean(s))

    return np.mean(metric)


def my_iou_metric(label, pred):
    return tf.py_func(get_iou_vector, [label, pred > 0.5], tf.float64)


def my_iou_metric_2(label, pred):
    return tf.py_func(get_iou_vector, [label, pred > 0], tf.float64)


def residual_block(block_input, num_filters=16, is_first_layer=False, is_decode=False):
    def basic_block(x):
        for i in range(2):
            if is_first_layer and i == 0:
                strides = 1
                padding = "same"
            else:
                strides = 1
                padding = "same"

            x = BatchNormalization()(x)
            x = Activation("relu")(x)
            x = Conv2D(num_filters, (3, 3), strides=strides, padding=padding)(x)
        return x

    x = basic_block(block_input)

    if K.int_shape(x) == K.int_shape(block_input):
        return Add()([x, block_input])
    else:
        # block_inputの形をxにあわせる
        if is_decode:
            w_x = Conv2D(num_filters, (3, 3), padding="same")(block_input)
        else:
            w_x = Conv2D(num_filters, (3, 3), strides=1, padding="same")(block_input)
        return Add()([x, w_x])