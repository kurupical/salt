from keras.models import Model, load_model
from keras.layers import Input,Dropout,BatchNormalization,Activation,Add
from keras.layers.core import Lambda
from keras.layers.convolutional import Conv2D, Conv2DTranspose
from keras.layers.pooling import MaxPooling2D
from keras.layers.merge import concatenate


def build_model(size, start_neurons, activation, dropout_ratio, optimizer):

    input_layer = Input((size, size, 1))

    # standard size 128->64 custom 101->50
    conv1 = Conv2D(start_neurons * 1, (3, 3), activation=activation, padding="same")(input_layer)
    conv1 = Conv2D(start_neurons * 1, (3, 3), activation=activation, padding="same")(conv1)
    pool1 = MaxPooling2D((2, 2))(conv1)
    pool1 = Dropout(dropout_ratio)(pool1)

    # standard size 64->32 custom 50->25
    conv2 = Conv2D(start_neurons * 2, (3, 3), activation=activation, padding="same")(pool1)
    conv2 = Conv2D(start_neurons * 2, (3, 3), activation=activation, padding="same")(conv2)
    pool2 = MaxPooling2D((2, 2))(conv2)
    pool2 = Dropout(dropout_ratio)(pool2)

    # standard size 32->16 custom 25->12
    conv3 = Conv2D(start_neurons * 4, (3, 3), activation=activation, padding="same")(pool2)
    conv3 = Conv2D(start_neurons * 4, (3, 3), activation=activation, padding="same")(conv3)
    pool3 = MaxPooling2D((2, 2))(conv3)
    pool3 = Dropout(dropout_ratio)(pool3)

    # standard size 16->8 custom 12->6
    conv4 = Conv2D(start_neurons * 8, (3, 3), activation=activation, padding="same")(pool3)
    conv4 = Conv2D(start_neurons * 8, (3, 3), activation=activation, padding="same")(conv4)
    pool4 = MaxPooling2D((2, 2))(conv4)
    pool4 = Dropout(dropout_ratio)(pool4)

    # Middle
    convm = Conv2D(start_neurons * 16, (3, 3), activation=activation, padding="same")(pool4)
    convm = Conv2D(start_neurons * 16, (3, 3), activation=activation, padding="same")(convm)

    # standard 8->16, custom 6->12
    deconv4 = Conv2DTranspose(start_neurons * 8, (3, 3), strides=(2, 2), padding="same")(convm)
    uconv4 = concatenate([deconv4, conv4])
    uconv4 = Dropout(dropout_ratio)(uconv4)
    uconv4 = Conv2D(start_neurons * 8, (3, 3), activation=activation, padding="same")(uconv4)
    uconv4 = Conv2D(start_neurons * 8, (3, 3), activation=activation, padding="same")(uconv4)

    # standard 16->32 custom 12->25
    deconv3 = Conv2DTranspose(start_neurons * 4, (3, 3), strides=(2, 2), padding="valid")(uconv4)
    uconv3 = concatenate([deconv3, conv3])
    uconv3 = Dropout(dropout_ratio)(uconv3)
    uconv3 = Conv2D(start_neurons * 4, (3, 3), activation=activation, padding="same")(uconv3)
    uconv3 = Conv2D(start_neurons * 4, (3, 3), activation=activation, padding="same")(uconv3)

    # standard 32->64 custom 25->50
    deconv2 = Conv2DTranspose(start_neurons * 2, (3, 3), strides=(2, 2), padding="same")(uconv3)
    uconv2 = concatenate([deconv2, conv2])
    uconv2 = Dropout(dropout_ratio)(uconv2)
    uconv2 = Conv2D(start_neurons * 2, (3, 3), activation=activation, padding="same")(uconv2)
    uconv2 = Conv2D(start_neurons * 2, (3, 3), activation=activation, padding="same")(uconv2)

    # standard 64->128 custom 50->101
    deconv1 = Conv2DTranspose(start_neurons * 1, (3, 3), strides=(2, 2), padding="valid")(uconv2)
    uconv1 = concatenate([deconv1, conv1])
    uconv1 = Dropout(dropout_ratio)(uconv1)
    uconv1 = Conv2D(start_neurons * 1, (3, 3), activation=activation, padding="same")(uconv1)
    uconv1 = Conv2D(start_neurons * 1, (3, 3), activation=activation, padding="same")(uconv1)

    output_layer = Conv2D(1, (1, 1), padding="same", activation="sigmoid")(uconv1)

    model = Model(input_layer, output_layer)
    model.compile(loss="binary_crossentropy", optimizer=optimizer, metrics=["accuracy"])

    return model