from keras.models import Model, load_model
from keras.layers import Input,Dropout,BatchNormalization,Activation,Add
from keras.layers.core import Lambda
from keras.layers.convolutional import Conv2D, Conv2DTranspose
from keras.layers.pooling import MaxPooling2D
from keras.layers.merge import concatenate

from src.layer.loss import get_loss
from src.layer.optimizer import get_optimizer
from src.layer.common import *

from segmentation_models import Unet

def build_model(size, optimizer, loss):
    model = Unet(input_shape=(size, size, 3), backbone_name="resnet34", encoder_weights="imagenet")
    model.compile(loss=get_loss(loss), optimizer=get_optimizer(optimizer), metrics=["accuracy", my_iou_metric])

    return model