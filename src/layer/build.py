
from src.layer import unet_101to6, \
    unet_101to6_resnet, unet_101to6_shake, unet_101to6_resnet_kernel, unet_101to6_resnet34, pretrain_resnet50, \
    unet_pretrain, pretrain_se_resnet50, pretrain_se_resnet50_salt_exists, pretrain_se_resnet50_drop2, \
    pretrain_se_resnet34, pretrain_se_resnet152
from src.layer.common import *
from src.layer.loss import *
from src.layer.optimizer import *
from keras.models import load_model
from copy import copy
from keras.utils.training_utils import multi_gpu_model

def load_pretrain_model_lovasz(param):
    model = load_model(param["path"], custom_objects={'keras_lovasz_hinge': keras_lovasz_hinge,
                                                      'lovasz_hinge': lovasz_loss,
                                                      'focal_loss': focal_loss,
                                                      'bce_dice_loss': bce_dice_loss,
                                                      'my_iou_metric': my_iou_metric,
                                                      'my_iou_metric_2': my_iou_metric_2})
    input_layer = model.layers[0].input

    output_layer = model.layers[-1].input

    model = Model(input_layer, output_layer)
    gpu_count = copy(param["gpu_count"])
    if gpu_count > 1:
        print("multi_gpu: {}".format(gpu_count))
        model = multi_gpu_model(model, gpu_count)

    model.compile(loss=get_loss(param["loss"]),
                  optimizer=get_optimizer(param["optimizer"]),
                  metrics=["accuracy", my_iou_metric_2])

    return model

def build_model(name, param):
    if name == "unet_101to6":
        return unet_101to6.build_model(**param)
    if name == "unet_101to6_resnet":
        return unet_101to6_resnet.build_model(**param)
    if name == "unet_101to6_resnet_kernel":
        return unet_101to6_resnet_kernel.build_model(**param)
    if name == "unet_101to6_shake":
        return unet_101to6_shake.build_model(**param)
    if name == "unet_101to6_resnet34":
        return unet_101to6_resnet34.build_model(**param)
    if name == "pretrain_resnet50":
        return pretrain_resnet50.build_model(**param)
    if name == "pretrain_se_resnet50":
        return pretrain_se_resnet50.build_model(**param)
    if name == "load_pretrain_model":
        return load_pretrain_model_lovasz(param)
    if name == "unet_pretrain":
        return unet_pretrain.build_model(**param)
    if name == "pretrain_se_resnet50_salt_exists":
        return pretrain_se_resnet50_salt_exists.build_model(**param)
    if name == "pretrain_se_resnet50_drop2":
        return pretrain_se_resnet50_drop2.build_model(**param)
    if name == "pretrain_se_resnet34":
        return pretrain_se_resnet34.build_model(**param)
    if name == "pretrain_se_resnet152":
        return pretrain_se_resnet152.build_model(**param)

    if name == "pretrain_resnet50_pytorch":
        return pretrain_resnet_pytorch.resnet50(pretrained=True, **param)