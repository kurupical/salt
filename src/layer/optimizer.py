from keras import optimizers


def get_optimizer(param):
    name = param["name"]
    if name == "adam":
        return optimizers.Adam(**param["param"])
    if name == "sgd":
        return optimizers.sgd(**param["param"])
