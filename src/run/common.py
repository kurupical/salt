import numpy as np

img_size_ori = 101
from skimage.transform import resize
from src.preprocess.argumentation import *

def upsample(img, img_size_target):
    if img_size_ori == img_size_target:
        return img
    return resize(img, (img_size_target, img_size_target), mode='constant', preserve_range=True)
    # res = np.zeros((img_size_target, img_size_target), dtype=img.dtype)
    # res[:img_size_ori, :img_size_ori] = img
    # return res


def downsample(img):
    return resize(img, (img_size_ori, img_size_ori), mode='constant', preserve_range=True)
    # return img[:img_size_ori, :img_size_ori]


def iou(img_true, img_pred):
    i = np.sum((img_true * img_pred) > 0)
    u = np.sum((img_true + img_pred) > 0)
    if u == 0:
        return u
    return i / u


def iou_metric(imgs_true, imgs_pred):
    iou_thresholds = np.array([0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95])

    num_images = len(imgs_true)
    scores = np.zeros(num_images)

    for i in range(num_images):
        if imgs_true[i].sum() == imgs_pred[i].sum() == 0:
            scores[i] = 1
        else:
            scores[i] = (iou_thresholds <= iou(imgs_true[i], imgs_pred[i])).mean()

    return scores.mean()

def predict_result(model,x_test,img_size_target): # predict both orginal and reflect x
    preds_test = model.predict(x_test).reshape(-1, img_size_target, img_size_target)
    preds_test += np.array([ np.fliplr(a) for a in model.predict(np.array([np.fliplr(x) for x in x_test])).reshape(-1, img_size_target, img_size_target)])
    return preds_test/2.0

def rle_encode(im):
    pixels = im.flatten(order = 'F')
    pixels = np.concatenate([[0], pixels, [0]])
    runs = np.where(pixels[1:] != pixels[:-1])[0] + 1
    runs[1::2] -= runs[::2]
    return ' '.join(str(x) for x in runs)

def resize_image(X, img_size, resize_mode, is_valid=False, is_mask_zeroone=False):
    if resize_mode == "normal":
        image = np.array([upsample(x, img_size) for x in X]).reshape(X.shape[0], img_size, img_size, -1)
        if is_mask_zeroone:
            image = (image > 0.5).astype(np.float32)
            return image
        else:
            return image
    if resize_mode == "pad_to_factor":
        if is_valid:
            return unpad(X, img_size)
        else:
            if img_size == 128:
                return np.array([argumentation(x, "pad_to_factor", {"border": [13, 14]}) for x in X]).reshape(X.shape[0], 128, 128, -1)
            if img_size == 256:
                X = np.resize([np.array([upsample(x, 202) for x in X]).reshape(-1, 202, 202, 1)])
            return np.array([argumentation(x, "pad_to_factor", {"border": [27]}) for x in X]).reshape(X.shape[0], 256, 256, -1)
    return X.reshape(-1, img_size, img_size, 1)

def visualize_acc(pred, true, output_path):
    ary = []
    pred = pred.reshape(-1)
    true = true.reshape(-1)
    adjust_pos = 50
    for i in range(102):
        w_idx = np.where(((i - 1) * 101 < true) & (true <= i * 101))
        if len(w_idx[0]) > 0:
            w_pred = (pred[w_idx] > 0)
            w_true = (true[w_idx].reshape(-1) > 0)

            acc = np.sum((w_pred == w_true)) / len(w_pred)
            ary.append([(i - 1) * 101 + adjust_pos, acc])
        else:
            pass
            # ary.append([i*101+adjust_pos, 0])
    ary = np.array(ary)

    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()

    ax1.hist(pred, bins=102, range=(0, 10201), label="freq")
    ax2.plot(ary[:, 0], ary[:, 1], color="red", label="acc")
    ax1.set_ylabel("frequency")
    ax2.set_ylabel("accuracy")
    ax1.tick_params(axis='y', colors="blue")
    ax2.tick_params(axis='y', colors="red")
    plt.savefig(output_path)