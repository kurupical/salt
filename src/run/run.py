import numpy as np
import pandas as pd

from random import randint

import matplotlib.pyplot as plt
plt.style.use('seaborn-white')
import seaborn as sns
sns.set_style("white")
from keras.preprocessing.image import load_img
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras import backend as K

from sklearn.model_selection import train_test_split
from skimage.transform import resize
import json
from datetime import datetime as dt
import os
import pickle
from copy import copy
import shutil
import random as rn
import tensorflow as tf

from src.layer.build import build_model
from src.run.common import *
from src.preprocess.argumentation import *

#####################################
# 定数
#####################################
data_dir = "../../data/original"

# json_path = "../../config/unet_101to6.json"
# json_path = "../../config/unet_101to6_resnet.json"
# json_path = "../../config/unet_101to6_shake.json"
json_path = "../../config/unet_101to6_resnet_kernel.json"
argumentation_dir = "../../data/edited/argumentation"

#####################################
# 前処理
#####################################
with open(json_path, "r") as f:
    params = json.load(f)

img_size = copy(params["model_detail"]["size"])
random_state = copy(params["random_state"])
filter_num = copy(params["filter"])

os.environ["PYTHONHASHSEED"] = str(random_state)
np.random.seed(random_state)
rn.seed(random_state)
tf.set_random_seed(random_state)

#####################################
# データ前処理（あとで関数化）
#####################################

# train/testのID読み込み
df_depth = pd.read_csv("{}/depths.csv".format(data_dir), index_col="id")
df_depth.columns = ["depth"]
df_train = pd.read_csv("{}/train.csv".format(data_dir), index_col="id", usecols=[0])
df_submit = df_depth[~df_depth.index.isin(df_train.index)]
# df_train = df_train.head(30) # test

# train/testの画像読み込み
print("*** load datasets ***")
df_train["images"] = [np.array(load_img("{}/train/images/{}.png".format(data_dir, idx), grayscale=True)) / 255 for idx in df_train.index]
df_train["masks"] = [np.array(load_img("{}/train/masks/{}.png".format(data_dir, idx), grayscale=True)) / 255 for idx in df_train.index]
df_train = pd.concat([df_train, df_depth], axis=1, join="inner")
df_submit["images"] = [np.array(load_img("{}/test/images/{}.png".format(data_dir, idx), grayscale=True)) / 255 for idx in df_submit.index]

ids_train, ids_test, x_train, x_test, y_train, y_test, depth_train, depth_test = train_test_split(
    df_train.index.values,
    np.array(df_train.images.tolist()).reshape(-1, img_size, img_size, 1),
    np.array(df_train.masks.tolist()).reshape(-1, img_size, img_size, 1),
    np.array(df_train.depth.tolist()),
    test_size=0.2,
    random_state=random_state)

# data argumentation
print("*** data argumentation ***")
x_train_org = x_train
# y_train_org = np.array([filter_image(x, filter) for x in y_train])
y_train_org = y_train
for arg in params["argumentations"]:
    print(arg)
    x_train = np.append(x_train, [x for x in argumentation(x_train_org, arg)], axis=0)
    # y_train = np.append(y_train, [filter_image(x, filter) for x in argumentation(y_train_org, arg)], axis=0)
    y_train = np.append(y_train, [x for x in argumentation(y_train_org, arg)], axis=0)


print("x_train.shape: {}".format(x_train.shape))
#####################################
# モデル作成〜学習
#####################################
model = build_model(name=params["model"]["name"],
                    param=params["model_detail"])

model.summary()

output_dir = "../../output/{}".format(dt.now().strftime("%Y%m%d%H%M%S"))
os.makedirs(output_dir)

early_stopping = EarlyStopping(patience=params["training"]["patience"])
#model_checkpoint = ModelCheckpoint("{}/model-{epoch:02d}-{val_acc:.2f}.hdf5",
"""
model_checkpoint = ModelCheckpoint(output_dir + "/model-{epoch:02d}-{val_acc:.2f}.hdf5",
                                   save_best_only=True,
                                   verbose=1)
"""
reduce_lr = ReduceLROnPlateau(factor=0.5, patience=5, min_lr=0.000001)

history = model.fit(x_train, y_train,
                    validation_data=[x_test, y_test],
                    epochs=params["training"]["epochs"],
                    batch_size=params["training"]["batch_size"],
                    verbose=0,
#                    callbacks=[early_stopping, model_checkpoint, reduce_lr])
                    callbacks=[early_stopping, reduce_lr])

# 学習記録の出力
fig, (ax_loss, ax_acc) = plt.subplots(1, 2, figsize=(15, 5))
ax_loss.plot(history.epoch, history.history["loss"], label="Train loss")
ax_loss.plot(history.epoch, history.history["val_loss"], label="Validation loss")
ax_acc.plot(history.epoch, history.history["acc"], label="Train accuracy")
ax_acc.plot(history.epoch, history.history["val_acc"], label="Validation accuracy")

plt.savefig("{}/loss_graph.png".format(output_dir))
plt.close()

##################
# valid data
##################
def predict(model, X, TTAs, index=None):
    # normal
    df = pd.DataFrame(index=index)
    pred = model.predict(X).reshape(-1, img_size, img_size)

    # tta
    for tta in TTAs:
        w_X = argumentation(X, tta)
        w_pred = model.predict(w_X).reshape(-1, img_size, img_size)
        pred = pred + argumentation(w_pred, tta)

    pred = pred / (len(TTAs) + 1)
    df["preds"] = [x for x in pred]
    return pred, df

pred_test, df_pred_test = predict(model, x_test, copy(params["TTAs"]), ids_test)

with open("{}/depth_test_numpy.pickle".format(output_dir), "wb") as f:
    pickle.dump(depth_test, f)
with open("{}/x_test_id_numpy.pickle".format(output_dir), "wb") as f:
    pickle.dump(ids_test, f)
with open("{}/x_test_numpy.pickle".format(output_dir), "wb") as f:
    pickle.dump(x_test, f)
with open("{}/pred_test_numpy.pickle".format(output_dir), "wb") as f:
    pickle.dump(pred_test, f)
with open("{}/y_test_numpy.pickle".format(output_dir), "wb") as f:
    pickle.dump(y_test, f)

thresholds = np.array([0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95])
ious = np.array([iou_metric(y_test.reshape((-1, img_size, img_size)), [filter_image(img, filter_num) for img in pred_test > threshold]) for threshold in thresholds])

threshold_best_index = np.argmax(ious)
iou_best = ious[threshold_best_index]
threshold_best = thresholds[threshold_best_index]

plt.plot(thresholds, ious)
plt.plot(threshold_best, iou_best, "xr", label="Best threshold")
plt.xlabel("Threshold")
plt.ylabel("IoU")
plt.title("Threshold vs IoU ({}, {})".format(threshold_best, iou_best))
plt.legend()
plt.savefig("{}/iou.png".format(output_dir))

shutil.copy(json_path, "{}/param.json".format(output_dir))

x_submit = np.array([np.array(load_img("{}/test/images/{}.png".format(data_dir, idx), grayscale=True)) / 255 for idx in df_submit.index])
x_submit = x_submit.reshape(-1, img_size, img_size, 1)

pred_submit, _ = predict(model, x_submit, copy(params["TTAs"]))

with open("{}/pred_submit_numpy.pickle".format(output_dir), "wb") as f:
    pickle.dump(pred_submit, f)

pred_dict = {idx: rle_encode(filter_image(pred_submit[i] > threshold_best, filter_num)) for i, idx in enumerate(df_submit.index.values)}

sub = pd.DataFrame.from_dict(pred_dict,orient='index')
sub.index.names = ['id']
sub.columns = ['rle_mask']
sub.to_csv('{}/submission.csv'.format(output_dir))
