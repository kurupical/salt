import numpy as np
import pandas as pd

from random import randint

import matplotlib.pyplot as plt
plt.style.use('seaborn-white')
import seaborn as sns
sns.set_style("white")
from keras.preprocessing.image import load_img
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras import backend as K

from sklearn.model_selection import train_test_split
from skimage.transform import resize
import json
from datetime import datetime as dt
import os
from copy import copy

from src.layer.build import build_model
from src.run.common import *

#####################################
# 定数
#####################################
data_dir = "../../data/original"

json_path = "../../config/unet_101to6.json"

#####################################
# データ前処理（あとで関数化）
#####################################
with open(json_path, "r") as f:
    params = json.load(f)

img_size = copy(params["model_detail"]["size"])
# train/testのID読み込み
df_depth = pd.read_csv("{}/depths.csv".format(data_dir), index_col="id")

df_train = pd.read_csv("{}/train.csv".format(data_dir), index_col="id", usecols=[0])
df_submit = df_depth[~df_depth.index.isin(df_train.index)]
# df_train = df_train.head(30) # test

# train/testの画像読み込み
df_train["images"] = [np.array(load_img("{}/train/images/{}.png".format(data_dir, idx), grayscale=True)) / 255 for idx in df_train.index]
df_train["masks"] = [np.array(load_img("{}/train/masks/{}.png".format(data_dir, idx), grayscale=True)) / 255 for idx in df_train.index]
df_submit["images"] = [np.array(load_img("{}/test/images/{}.png".format(data_dir, idx), grayscale=True)) / 255 for idx in df_submit.index]

ids_train, ids_test, x_train, x_test, y_train, y_test = train_test_split(
    df_train.index.values,
    np.array(df_train.images.tolist()).reshape(-1, img_size, img_size, 1),
    np.array(df_train.masks.tolist()).reshape(-1, img_size, img_size, 1),
    test_size=0.2)

# data argumentation
x_train = np.append(x_train, [np.fliplr(x) for x in x_train], axis=0)
y_train = np.append(y_train, [np.fliplr(x) for x in y_train], axis=0)

#####################################
# モデル作成〜学習
#####################################
model = build_model(name=params["model"]["name"],
                    param=params["model_detail"])

model.summary()

output_dir = "../../output/{}".format(dt.now().strftime("%Y%m%d%H%M%S"))
os.makedirs(output_dir)

early_stopping = EarlyStopping(patience=params["training"]["patience"])
#model_checkpoint = ModelCheckpoint("{}/model-{epoch:02d}-{val_acc:.2f}.hdf5",
model_checkpoint = ModelCheckpoint(output_dir + "/model-{epoch:02d}-{val_acc:.2f}.hdf5",
                                   save_best_only=True,
                                   verbose=1)
reduce_lr = ReduceLROnPlateau(factor=0.5, patience=5, min_lr=0.000001)

history = model.fit(x_train, y_train,
                    validation_data=[x_test, y_test],
                    epochs=params["training"]["epochs"],
                    batch_size=params["training"]["batch_size"],
                    verbose=1,
                    callbacks=[early_stopping, model_checkpoint, reduce_lr])

# 学習記録の出力
fig, (ax_loss, ax_acc) = plt.subplots(1, 2, figsize=(15, 5))
ax_loss.plot(history.epoch, history.history["loss"], label="Train loss")
ax_loss.plot(history.epoch, history.history["val_loss"], label="Validation loss")
ax_acc.plot(history.epoch, history.history["acc"], label="Train accuracy")
ax_acc.plot(history.epoch, history.history["val_acc"], label="Validation accuracy")

plt.savefig("{}/loss_graph.png".format(output_dir))
plt.close()

pred_test = model.predict(x_test).reshape(-1, img_size, img_size)
pred_test = np.array([x for x in pred_test])

df_pred_test = pd.DataFrame(index=ids_test)
df_pred_test["preds"] = [x for x in pred_test]

# 評価
def filter_image(img):
    if img.sum() < 100:
        return np.zeros(img.shape)
    else:
        return img

thresholds = np.array([0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95])
ious = np.array([iou_metric(y_test.reshape((-1, img_size, img_size)), [filter_image(img) for img in pred_test > threshold]) for threshold in thresholds])

threshold_best_index = np.argmax(ious)
iou_best = ious[threshold_best_index]
threshold_best = thresholds[threshold_best_index]

plt.plot(thresholds, ious)
plt.plot(threshold_best, iou_best, "xr", label="Best threshold")
plt.xlabel("Threshold")
plt.ylabel("IoU")
plt.title("Threshold vs IoU ({}, {})".format(threshold_best, iou_best))
plt.legend()
plt.savefig("{}/iou.png".format(output_dir))

x_submit = np.array([np.array(load_img("{}/test/images/{}.png".format(data_dir, idx), grayscale=True)) / 255 for idx in df_submit.index])
x_submit = x_submit.reshape(-1, img_size, img_size, 1)
preds_submit = predict_result(model, x_submit, img_size)
pred_dict = {idx: rle_encode(filter_image(preds_submit[i] > threshold_best)) for i, idx in enumerate(df_submit.index.values)}

sub = pd.DataFrame.from_dict(pred_dict,orient='index')
sub.index.names = ['id']
sub.columns = ['rle_mask']
sub.to_csv('{}/submission.csv'.format(output_dir))
