#coding:utf-8
import numpy as np
import pandas as pd
import sys
sys.path.append("../../")
from random import randint
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
plt.style.use('seaborn-white')
import seaborn as sns
sns.set_style("white")
from keras.preprocessing.image import load_img
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras import backend as K
from keras.models import load_model
from keras.preprocessing.image import array_to_img
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import train_test_split
from skimage.transform import resize
import json
from datetime import datetime as dt
import os
import pickle
from copy import copy
import shutil
import random as rn
import tensorflow as tf
from sklearn.metrics import accuracy_score
import tensorflow as tf
from keras.utils.training_utils import multi_gpu_model

import gc
from src.layer.build import build_model
from src.run.common import *
from src.preprocess.argumentation import *
from src.layer.loss import keras_lovasz_hinge, focal_loss, bce_dice_loss, lovasz_loss
from src.layer.common import *
from src.layer.callback import *


def main(json_path, isDebug=False, is_run_once=True, kfold_iter=None):
    #####################################
    # 定数
    #####################################
    data_dir = "../../data/original"

    argumentation_dir = "../../data/edited/argumentation"

    n_splits = 5

    #####################################
    # 前処理
    #####################################
    with open(json_path, "r") as f:
        params = json.load(f)

    img_size = copy(params["model_detail"]["size"])
    loss_function = copy(params["model_detail"]["loss"])
    random_state = copy(params["random_state"])
    filter_num = copy(params["filter"])
    channels = copy(params["channel"])
    prediction_mode = copy(params["prediction_mode"])
    resize_mode = copy(params["resize_mode"])
    is_mask_zeroone = copy(params["mask_zeroone"])
    gpu_count = copy(params["model_detail"]["gpu_count"])

    os.environ["PYTHONHASHSEED"] = str(random_state)
    np.random.seed(random_state)
    rn.seed(random_state)
    tf.set_random_seed(random_state)
    output_dir = "../../output/{}".format(dt.now().strftime("%Y%m%d%H%M%S"))
    os.makedirs(output_dir)
    shutil.copy(json_path, "{}/param.json".format(output_dir))

    #####################################
    # データ前処理（あとで関数化）
    #####################################

    # train/testのID読み込み
    df_depth = pd.read_csv("{}/depths.csv".format(data_dir), index_col="id")
    df_depth.columns = ["depth"]
    df_train = pd.read_csv("{}/train.csv".format(data_dir), index_col="id", usecols=[0])
    df_submit = df_depth[~df_depth.index.isin(df_train.index)]
    if isDebug:
        df_submit = df_submit.head(30)
        df_train = df_train.head(30)
        params["training"]["epochs"] = 2
    # train/testの画像読み込み
    print("*** load datasets ***")
    df_train["images"] = [np.array(load_img("{}/train/images/{}.png".format(data_dir, idx), grayscale=True)) / 255 for
                          idx in df_train.index]
    df_train["masks"] = [np.array(load_img("{}/train/masks/{}.png".format(data_dir, idx), grayscale=True)) / 255 for idx
                         in df_train.index]
    df_submit["images"] = [np.array(load_img("{}/test/images/{}.png".format(data_dir, idx), grayscale=True)) / 255 for
                           idx in df_submit.index]
    df_train = pd.concat([df_train, df_depth], axis=1, join="inner")
    df_train["coverage"] = df_train.masks.map(np.sum) / pow(101, 2)

    def cov_to_class(val):
        for i in range(0, 11):
            if val * 10 <= i:
                return i

    df_train["coverage_class"] = df_train.coverage.map(cov_to_class)

    ids = df_train.index.values
    """
    if img_size > 101:
        X = np.array([upsample(x, img_size) for x in df_train.images]).reshape(-1, img_size, img_size, 1)
        y = np.array([upsample(x, img_size) for x in df_train.masks]).reshape(-1, img_size, img_size, 1)
        # X = np.array(df_train.images.map(upsample).tolist()).reshape(-1, img_size, img_size, 1)
        # y = np.array(df_train.masks.map(upsample).tolist()).reshape(-1, img_size, img_size, 1)
    else:
        X = np.array(df_train.images.tolist()).reshape(-1, img_size, img_size, 1)
        y = np.array(df_train.masks.tolist()).reshape(-1, img_size, img_size, 1)
    """
    X = np.array(df_train.images.tolist()).reshape(-1, 101, 101, 1)
    y = np.array(df_train.masks.tolist()).reshape(-1, 101, 101, 1)
    depth = np.array(df_train.depth.tolist())

    if channels == 2:
        X = argumentation(X,
                          name="depth",
                          param={"depth": depth})

    if channels == 3:
        X = np.stack((np.squeeze(X),) * 3, -1)

    fold = StratifiedKFold(n_splits=n_splits,
                           shuffle=True,
                           random_state=random_state)

    coverage_class = df_train.coverage_class.values
    submit_idx = df_submit.index.values

    x_submit = np.array(
        [np.array(load_img("{}/test/images/{}.png".format(data_dir, idx), grayscale=True)) / 255 for idx in submit_idx])
    x_submit = resize_image(x_submit, img_size, resize_mode, is_valid=True)

    if channels == 2:
        x_submit = argumentation(x_submit,
                                 name="depth",
                                 param={"depth": df_submit.depth.values})
    if channels == 3:
        x_submit = np.stack((np.squeeze(x_submit),) * 3, -1)

    del df_train, df_submit
    gc.collect()

    iter = 0
    result_ious = []
    pred_submit_total = None
    iou_score = 0

    for train_idx, test_idx in fold.split(X, coverage_class):
        with open(json_path, "r") as f:
            params = json.load(f)

        iter += 1
        if not kfold_iter is None:
            n_splits = 1
            if iter != kfold_iter:
                continue
        print("***********************")
        print("ITER {}".format(iter))
        print("***********************")
        ids_train = ids[train_idx]
        ids_test = ids[test_idx]
        x_train = X[train_idx]
        x_test = X[test_idx]
        y_train = y[train_idx]
        y_test = y[test_idx]
        depth_train = depth[train_idx]
        depth_test = depth[test_idx]

        output_dir_iter = "{}/iter{}".format(output_dir, iter)
        os.makedirs(output_dir_iter)

        # data argumentation
        print("*** data argumentation ***")
        x_train_org = x_train[:]
        # y_train_org = np.array([filter_image(x, filter) for x in y_train])
        y_train_org = y_train[:]

        x_train = resize_image(x_train, img_size, resize_mode)
        y_train = resize_image(y_train, img_size, resize_mode, is_mask_zeroone=is_mask_zeroone)
        x_test = resize_image(x_test, img_size, resize_mode, is_valid=True)
        y_test = resize_image(y_test, img_size, resize_mode, is_valid=True, is_mask_zeroone=is_mask_zeroone)

        x_train_org_reshaped = x_train[:]
        y_train_org_reshaped = y_train[:]

        image_num = 5

        fig, axs = plt.subplots(len(params["argumentations"]) + 2, 2 * image_num,
                                # + 1にしないのは、len()=0のときaxsが1次元になってややこしいから
                                figsize=(6 * image_num, (len(params["argumentations"]) + 1) * 4))

        not_empty_idx = np.where(np.sum(y_train, axis=(1, 2, 3)) > 0)[0][:image_num]
        if channels == 1:
            for j in range(image_num):
                axs[0, j * 2].imshow(array_to_img(x_train[not_empty_idx[j]]))
                axs[0, j * 2 + 1].imshow(array_to_img(y_train[not_empty_idx[j]]))
                axs[0, j * 2].set_title("original")

        i = 0
        for key in params["argumentations"].keys():
            i += 1
            print(key)
            x_shapes = (-1, x_train.shape[1], x_train.shape[2], x_train.shape[3])
            y_shapes = (-1, x_train.shape[1], x_train.shape[2], 1)
            if key in ["pad_to_factor", "pad_to_factor_edge"]:
                w_x = copy(x_train_org)
                w_y = copy(y_train_org)
            else:
                w_x = copy(x_train_org_reshaped)
                w_y = copy(y_train_org_reshaped)
            x_aug = np.array([x for x in argumentation(w_x, key, params["argumentations"][key], is_mask_zeroone=is_mask_zeroone)]).reshape(x_shapes)
            y_aug = np.array([x for x in argumentation(w_y, key, params["argumentations"][key], is_mask_zeroone=is_mask_zeroone, is_mask=True)]).reshape(y_shapes)
            x_train = np.append(x_train, x_aug, axis=0)
            # y_train = np.append(y_train, [filter_image(x, filter) for x in argumentation(y_train_org, arg)], axis=0)
            y_train = np.append(y_train, y_aug, axis=0)
            not_empty_idx = np.where(np.sum(y_aug, axis=(1, 2, 3)) > 0)[0][:image_num]
            if channels == 1:
                for j in range(image_num):
                    axs[i, j * 2].imshow(array_to_img(x_aug[not_empty_idx[j]]))
                    axs[i, j * 2 + 1].imshow(array_to_img(y_aug[not_empty_idx[j]]))
                    axs[i, j * 2].set_title(key)
        plt.savefig("{}/image_aug.png".format(output_dir_iter))
        print("x_train.shape: {}".format(x_train.shape))
        if prediction_mode == "exist":
            y_train = argumentation(y_train, "exists")
            y_test = argumentation(y_test, "exists")
        #####################################
        # モデル作成〜学習
        #####################################
        model = build_model(name=params["model"]["name"],
                            param=params["model_detail"])

        model.summary()

        """
        early_stopping = EarlyStopping(patience=params["training"]["patience"])
        #model_checkpoint = ModelCheckpoint("{}/model-{epoch:02d}-{val_acc:.2f}.hdf5",
        reduce_lr = ReduceLROnPlateau(factor=0.5, patience=5, min_lr=0.000001)
        """
        callbacks = []
        if gpu_count == 1:
            model_checkpoint = ModelCheckpoint(output_dir_iter + "/best.hdf5",
                                               save_best_only=True,
                                               verbose=1)
        else:
            model_checkpoint = MultiGPUCheckpointCallback(output_dir_iter + "/best.hdf5",
                                                          base_model=model,
                                                          save_best_only=True,
                                                          verbose=1)
        callbacks.append(model_checkpoint)

        for key, param in params["callback"].items():
            callbacks.extend(callback(key, param))
        print(callbacks)
        history = model.fit(x_train, y_train,
                            validation_data=[x_test, y_test],
                            epochs=params["training"]["epochs"] if not isDebug else 2,
                            batch_size=params["training"]["batch_size"],
                            verbose=1,
                            callbacks=callbacks)
        #                     callbacks=[early_stopping, reduce_lr])

        model = load_model("{}/best.hdf5".format(output_dir_iter),
                           custom_objects={'keras_lovasz_hinge': keras_lovasz_hinge,
                                           'lovasz_loss': lovasz_loss,
                                           'focal_loss': focal_loss,
                                           'bce_dice_loss': bce_dice_loss,
                                           'my_iou_metric': my_iou_metric,
                                           "my_iou_metric_2": my_iou_metric_2})

        del x_train, y_train
        gc.collect()

        # 学習記録の出力
        fig, (ax_loss, ax_acc) = plt.subplots(1, 2, figsize=(15, 5))
        ax_loss.plot(history.epoch, history.history["loss"], label="Train loss")
        ax_loss.plot(history.epoch, history.history["val_loss"], label="Validation loss")
        ax_acc.plot(history.epoch, history.history["acc"], label="Train accuracy")
        ax_acc.plot(history.epoch, history.history["val_acc"], label="Validation accuracy")

        plt.savefig("{}/loss_graph.png".format(output_dir_iter))
        plt.close()

        ##################
        # valid data
        ##################
        def predict(model, X, TTAs, prediction_mode, index=None):
            # normal
            df = pd.DataFrame(index=index)
            pred = model.predict(X)
            if prediction_mode == "normal":
                pred = pred.reshape(-1, img_size, img_size)
            else:
                pred = pred.reshape(-1, 1)
            # tta
            for key in TTAs.keys():
                w_X = argumentation(X, key, copy(TTAs[key]))
                w_pred = model.predict(w_X)
                if prediction_mode == "normal":
                    w_pred = w_pred.reshape(-1, img_size, img_size)
                    pred = pred + argumentation(w_pred, key, copy(TTAs[key]), reverse=True)
                else:
                    w_pred = w_pred.reshape(-1, 1)
                    pred = pred + w_pred

            pred = pred / (len(TTAs) + 1)
            df["preds"] = [x for x in pred]
            return pred, df

        gc.collect()
        pred_test, df_pred_test = predict(model, x_test, copy(params["TTAs"]), prediction_mode, ids_test)

        with open("{}/depth_test_numpy.pickle".format(output_dir_iter), "wb") as f:
            pickle.dump(depth_test, f)
        with open("{}/x_test_id_numpy.pickle".format(output_dir_iter), "wb") as f:
            pickle.dump(ids_test, f)
        with open("{}/x_test_numpy.pickle".format(output_dir_iter), "wb") as f:
            pickle.dump(x_test, f)
        with open("{}/pred_test_numpy.pickle".format(output_dir_iter), "wb") as f:
            pickle.dump(pred_test, f)
        with open("{}/y_test_numpy.pickle".format(output_dir_iter), "wb") as f:
            pickle.dump(y_test, f)

        thresholds = np.array([0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45,
                               0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95])
        if loss_function == "lovasz_loss":
            thresholds = np.log(thresholds / (1 - thresholds))

        if prediction_mode == "normal":
            ious = np.array([iou_metric(y_test.reshape((-1, img_size, img_size)),
                                        [filter_image(img, filter_num) for img in pred_test > threshold]) for threshold
                             in thresholds])
        else:
            ious = np.array(
                [accuracy_score(y_test, [pred for pred in pred_test > threshold]) for threshold in thresholds])

        threshold_best_index = np.argmax(ious)
        iou_best = ious[threshold_best_index]
        result_ious.append(iou_best)
        threshold_best = thresholds[threshold_best_index]

        plt.figure()
        plt.plot(thresholds, ious)
        plt.plot(threshold_best, iou_best, "xr", label="Best threshold")
        plt.xlabel("Threshold")
        plt.ylabel("IoU")
        plt.title("Threshold vs IoU ({}, {})".format(threshold_best, iou_best))
        plt.legend()
        plt.savefig("{}/iou.png".format(output_dir_iter))

        if prediction_mode == "normal":
            pred_test_best = np.array([filter_image(img, filter_num) for img in pred_test > threshold_best])
        else:
            pred_test_best = np.array([pred for pred in pred_test > threshold_best])
        with open("{}/pred_test_best_numpy.pickle".format(output_dir_iter), "wb") as f:
            pickle.dump(pred_test_best, f)

        # accuracy <MASK OR NOT>
        if prediction_mode == "normal":
            pred_masks = np.sum(pred_test_best, axis=(1, 2))
            true_masks = np.sum(y_test, axis=(1, 2))

            visualize_acc(pred=pred_masks, true=true_masks, output_path="{}/acc_visualize.png".format(output_dir_iter))

        pred_submit, _ = predict(model, x_submit, copy(params["TTAs"]), prediction_mode)

        if prediction_mode == "normal":
            if img_size > 101:
                pred_submit = np.array([downsample(x) for x in pred_submit])

        if prediction_mode == "normal":
            pred_submit_best = np.array([filter_image(img, filter_num) for img in pred_submit > threshold_best])
        else:
            pred_submit_best = np.array([pred for pred in pred_submit > threshold_best]).astype(int)

        with open("{}/pred_submit_numpy.pickle".format(output_dir_iter), "wb") as f:
            pickle.dump(pred_submit_best, f)

        if pred_submit_total is None:
            pred_submit_total = pred_submit_best
        else:
            pred_submit_total = pred_submit_total + pred_submit_best

        iou_score += ious[threshold_best_index]
        if is_run_once:
            n_splits = 1
            break

    pred_submit_total = pred_submit_total / n_splits

    if prediction_mode == "normal":
        pred_dict = {idx: rle_encode(filter_image(pred_submit_total[i] > 0.5, filter_num)) for i, idx in
                     enumerate(submit_idx)}
        with open("{}/submission_numpy.pickle".format(output_dir), "wb") as f:
            pickle.dump(filter_image(pred_submit_total[i] > 0.5, filter_num), f)
    else:
        pred_dict = {idx: (pred_submit_total[i] > 0.5).astype(int) for i, idx in enumerate(submit_idx)}
    with open("{}/result.txt".format(output_dir), "w") as f:
        f.write("IOU: {}\n".format(result_ious))
        f.write("IOU_MEAN: {}".format(np.array(result_ious).mean()))
    sub = pd.DataFrame.from_dict(pred_dict, orient='index')
    sub.index.names = ['id']
    sub.columns = ['rle_mask']
    sub.to_csv('{}/submission.csv'.format(output_dir))

    return iou_score / n_splits


if __name__ == "__main__":
    json_details = [
        # ["../../config/test/v1.1_augmentation_and_lr/base.json", True, None],
        ["../../config/test/pg_test/resnet50.json", True, None],
    ]
    log_dir = "../../output/log/{}.log".format(dt.now().strftime("%Y%m%d%H%M%S"))

    # check json
    for json_detail in json_details:
        with open(json_detail[0], "r") as f:
            print(json_detail[0])
            params = json.load(f)

    df_iou_scores = pd.DataFrame(columns=["json", "score"])
    now = dt.now().strftime("%Y%m%d%H%M%S")
    for json_detail in json_details:
        iou_score = main(json_detail[0], isDebug=True, is_run_once=json_detail[1], kfold_iter=json_detail[2])
        w_df = pd.DataFrame()
        w_df["json"] = [json_detail[0]]
        w_df["score"] = [iou_score]

        df_iou_scores = pd.concat([df_iou_scores, w_df])
        df_iou_scores.to_csv("../../output/log/{}.csv".format(now))
