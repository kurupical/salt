import pandas as pd
import numpy as np
predict_normal_path = "../../src/work/submission/normal.csv"
predict_exists_path = "../../src/work/submission/mask.csv"
output_path = "../../src/work/submission/merge.csv"
df_normal = pd.read_csv(predict_normal_path, index_col=0)
df_exists = pd.read_csv(predict_exists_path, index_col=0)

print(df_exists)

df_output = df_normal[:]
df_output["rle_mask"] = [normal.tolist()[0] if exists else "" for normal, exists in zip(df_normal.values, df_exists.values)]

print("*** normal ***")
print(df_normal.iloc[:30])
print("*** exists ***")
print(df_exists.iloc[:30])
print("*** normal ***")
print(df_output.iloc[:30])

df_output.to_csv(output_path)