
import pandas as pd
import numpy as np
from skimage.transform import rescale
import random
import pickle
from keras.preprocessing.image import load_img
import cv2
from skimage.transform import resize
import src.run.common
import numpy as np
from scipy.ndimage.interpolation import map_coordinates
from scipy.ndimage.filters import gaussian_filter
import matplotlib.pyplot as plt
from keras.preprocessing.image import array_to_img

def horizon_flip(X):
    # data argumentation
    return np.fliplr(X)

def vertical_flip(X):
    return np.flipud(X)

def inverse_color(X):
    return 1 - X

def filter_image(img, threshold):
    if img.sum() < threshold:
        return np.zeros(img.shape)
    else:
        return img

def do_shift_scale_crop(image, x0=0, y0=0, x1=1, y1=1, is_mask=False, is_mask_zeroone=False):
    height, width = image.shape[:2]
    image = image[y0:y1, x0:x1]
    scaled_height = y1 - y0
    scaled_width = x1 - x0

    image = rescale(image, (height/scaled_height, width/scaled_width))
    if is_mask and is_mask_zeroone:
        image = (image>0.5).astype(np.float32)
    return image

def do_random_shift_scale_crop_pad(image, limit=0.10, is_mask=False, is_mask_zeroone=False):
    H, W = image.shape[:2]

    dy = int(H*limit)
    y0 =   np.random.randint(0,dy)
    y1 = H-np.random.randint(0,dy)

    dx = int(W*limit)
    x0 =   np.random.randint(0,dx)
    x1 = W-np.random.randint(0,dx)

    #y0, y1, x0, x1
    image = do_shift_scale_crop(image,x0, y0, x1, y1, is_mask, is_mask_zeroone)
    return image

def do_brightness_shift(image, alpha=0.125, reverse=False):
    if reverse:
        return image - alpha
    else:
        image = image + alpha
        image = np.clip(image, 0, 1)
    return image


def do_brightness_multiply(image, alpha=1, reverse=False):
    if reverse:
        return alpha/image
    else:
        image = alpha*image
        image = np.clip(image, 0, 1)
        return image


def add_depth(image, depth):
    """
    depthを加える
    :param image:
    :param depth:
    :return:
    """

    w_depth = np.arange(depth, depth+image.shape[0]).reshape(-1, 1)
    w_depth = np.tile(w_depth, (image.shape[0])).reshape(image.shape[0], image.shape[0], 1)
    w_depth = w_depth / 1000

    return np.concatenate((image, w_depth), axis=2)

def do_random_pad_to_factor(image, border):
    image = cv2.copyMakeBorder(image, border[0], border[1], border[0], border[1], cv2.BORDER_REFLECT_101)

    return image


def do_random_pad_to_factor_edge(image, border):
    image = cv2.copyMakeBorder(image, border[0], border[1], border[0], border[1], cv2.BORDER_REPLICATE)

    return image


def unpad(image, img_size):
    if img_size == 101:
        return image
    if img_size == 128:
        prob = np.array([do_random_pad_to_factor(x, [13, 14]) for x in image])
        prob = prob[:, 14:14+101, 13:13+101]
    if img_size == 256:
        prob = np.array([do_random_pad_to_factor(x, [27, 27]) for x in image])
        prob = prob[:, 27:27+202, 27:27+202]
    prob = np.array([src.run.common.upsample(x, img_size) for x in prob]).reshape(image.shape[0], img_size, img_size, -1)
    return prob[:, :, ::-1]

def elastic_transform(image, alpha, sigma, random_state=None, is_mask=False, is_mask_zeroone=False):
    """Elastic deformation of images as described in [Simard2003]_.
    .. [Simard2003] Simard, Steinkraus and Platt, "Best Practices for
       Convolutional Neural Networks applied to Visual Document Analysis", in
       Proc. of the International Conference on Document Analysis and
       Recognition, 2003.
    """
    if random_state is None:
        random_state = np.random.RandomState(None)

    shape = image.shape
    dx = gaussian_filter((random_state.rand(*shape) * 2 - 1), sigma, mode="constant", cval=0) * alpha
    dy = gaussian_filter((random_state.rand(*shape) * 2 - 1), sigma, mode="constant", cval=0) * alpha
    dz = np.zeros_like(dx)

    x, y, z = np.meshgrid(np.arange(shape[0]), np.arange(shape[1]), np.arange(shape[2]))
    indices = np.reshape(y+dy, (-1, 1)), np.reshape(x+dx, (-1, 1)), np.reshape(z, (-1, 1))

    distored_image = map_coordinates(image, indices, order=1, mode='reflect')
    if is_mask and is_mask_zeroone:
        distored_image = (image>0.5).astype(np.float32)

    return distored_image.reshape(image.shape)

def mixup(image, alpha=0.2, generate_num=None, is_debug=False):
    if generate_num is None:
        generate_num = len(image)

    rand_idx1 = np.random.randint(0, len(image), size=generate_num)
    rand_idx2 = np.random.randint(0, len(image), size=generate_num)

    output = []
    for idx1, idx2 in zip(rand_idx1, rand_idx2):
        lambda_ = np.random.beta(alpha, alpha)
        x = lambda_ * image[idx1] + (1 - lambda_) * image[idx2]
        output.append(x)

    if is_debug:
        plt.figure()
        plt.imshow(array_to_img(image[idx1]))
        plt.savefig("1.jpg")
        plt.imshow(array_to_img(image[idx2]))
        plt.savefig("2.jpg")
        plt.imshow(array_to_img(x))
        plt.savefig("3.jpg")

    return np.array(output)

def is_exists_salt(image):
    return np.array([np.any(x) for x in image]).astype(int)

def argumentation(X, name, param=None, is_mask=False, is_mask_zeroone=False, reverse=False):
    if name == "horizon_flip":
        return np.array([horizon_flip(x) for x in X])
    if name == "vertical_flip":
        return np.array([vertical_flip(x) for x in X])
    if name == "inverse_color":
        return inverse_color(X)
    if name == "random_crop":
        np.random.seed(param["seed"])
        return np.array([do_random_shift_scale_crop_pad(x, is_mask=is_mask, is_mask_zeroone=param["mask_zeroone"]) for x in X])
    if name == "brightness_shift":
        if is_mask:
            return X
        else:
            return np.array([do_brightness_shift(x, reverse=reverse) for x in X])
    if name == "brightness_multiply":
        if is_mask:
            return X
        else:
            return np.array([do_brightness_multiply(x, reverse=reverse) for x in X])
    if name == "depth":
        return np.array([add_depth(x, depth) for x, depth in zip(X, param["depth"])])
    if name == "pad_to_factor":
        return np.array([do_random_pad_to_factor(x, param["border"]) for x in X])
    if name == "pad_to_factor_edge":
        return np.array([do_random_pad_to_factor_edge(x, param["border"]) for x in X])
    if name == "unpad":
        return unpad(X)
    if name == "elastic_transform":
        np.random.seed(param["seed"])
        return np.array([elastic_transform(x, x.shape[1] * 2, x.shape[1] * 0.08, is_mask=is_mask, is_mask_zeroone=param["mask_zeroone"]) for x in X])
    if name == "mixup":
        np.random.seed(param["seed"])
        return mixup(X, alpha=param["alpha"])
    if name == "exists":
        return is_exists_salt(X)


    raise NameError("this name {} is not existed".format(name))
